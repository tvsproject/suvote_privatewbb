/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.core;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/**
 * Executor that executes items in serial order. This is used to ensure that messages that are received that relate to same serial
 * number are executed in serial, never concurrently, whilst using a single shared executor for all requests. This allows unrelated
 * messages to execute concurrently.
 * 
 * @author Chris Culnane
 * 
 */
public class SerialExecutor implements Executor {

  /**
   * Queue of executable tasks
   */
  private final Queue<Runnable> tasks = new ArrayDeque<Runnable>();

  /**
   * Executor to run the actual tasks on
   */
  private final Executor        executor;
  /**
   * Runnable of the currently active task
   */
  private Runnable              active;

  /**
   * Creates a new SerialExecutor that wraps the specified Executor
   * 
   * @param executor
   *          Executor to use to perform the actual execution
   */
  public SerialExecutor(Executor executor) {
    this.executor = executor;
  }

  /**
   * Executes the specified task in order that it was received, waiting for prior tasks to complete first. The task is added to the
   * queue and if no previous tasks are active the next task is scheduled
   */
  public synchronized void execute(final Runnable r) {
    tasks.offer(new Runnable() {

      // Create new runnable object to execute on the underlying executor
      public void run() {
        try {
          // Run the actual task
          r.run();
        }
        finally {
          // Always schedule the next task even if the previous failed
          scheduleNext();
        }
      }
    });
    // If no tasks are running we schedule the first task to run
    if (active == null) {
      scheduleNext();
    }
  }

  /**
   * Schedules the next task to run by polling the queue and executing it
   */
  protected synchronized void scheduleNext() {
    if ((active = tasks.poll()) != null) {
      executor.execute(active);
    }
  }
}
