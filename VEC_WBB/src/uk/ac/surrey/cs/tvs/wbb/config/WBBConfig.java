/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDef;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * Interface for WBBConfig to generalise usage
 * 
 * @author Chris Culnane
 * 
 */
public interface WBBConfig {

  public static final String ADDRESS                                   = "address";
  public static final String BALLOT_TIMEOUT                            = "ballotTimeout";
  public static final String BASE_ENCRYPTED_IDS_FILE                   = "baseEncryptedIds";
  public static final String BUFFER_BOUND                              = "bufferBound";
  public static final String CANDIDATE_COUNT                           = "candidates";
  public static final String CERT_STORES_SPEC                          = "certStores";
  public static final String CERTIFICATE_KEYSTORE                      = "certificateKeyStore";
  public static final String CERTIFICATE_KEYSTORE_PASSWORD             = "certificateKeyStorePwd";
  public static final String COMMIT_DIRECTORY                          = "commitDirectory";
  public static final String COMMIT_TIME                               = "commitTime";
  public static final String COMMIT_UPLOAD_ATTACHMENT_DIRECTORY_PREFIX = "CommitAttachments";
  public static final String COMMIT_UPLOAD_ATTACHMENT_EXTENSION        = ".zip";
  public static final String COMMIT_UPLOAD_ATTACHMENT_PREFIX           = "CommitUploadAttachment";
  public static final String COMMIT_UPLOAD_DIR                         = "commitUploadDirectory";
  public static final String CRL_FOLDER                                = "CRLFolder";
  public static final String DEFAULT_MESSAGE_DIGEST                    = SystemConstants.DEFAULT_MESSAGE_DIGEST;
  public static final String DISTRICT_CONFIG                           = "districtConf";
  public static final String ELECTION_PUBLIC_KEY                       = "electionPublicKey";
  public static final String EXTERNAL_PORT                             = "extPort";
  public static final String FILE_UPLOAD_TIMEOUT                       = "fileUploadTimeout";
  public static final String HOST_ADDRESS                              = "address";
  public static final String HOST_PORT                                 = "port";
  public static final String KEYSTORE_PASSWORD                         = "keyStorePwd";
  public static final String LIVE_CHECK_POD                            = "liveCheckPOD";
  public static final String MAX_ZIP_ENTRY                             = "max_zip_entry";
  /**
   * Configuration fields.
   */
  public static final String MESSAGE_DIGEST                            = "messageDigest";
  public static final String PATH_TO_KEYSTORE                          = "pathToKeyStore";
  public static final String PEER_ID                                   = "id";
  public static final String PEERS                                     = "peers";
  public static final String PORT                                      = "port";
  public static final String PUBLIC_WBB_ADDRESS                        = "publicWBBAddress";
  public static final String PUBLIC_WBB_PORT                           = "publicWBBPort";
  public static final String PUBLIC_WBB_RETRY                          = "publicWBBRetry";
  public static final String RACES                                     = "races";
  public static final String RUN_COMMIT_AT                             = "runCommitAt";
  public static final String SCHEMA_LIST                               = "schemaList";
  /**
   * Static variable to point towards schema for the config file
   */
  public static final String SCHEMA_LOCATION                           = "./schemas/wbbconfigschema.json";
  public static final String SIGNING_KEY_ALIAS                         = "alias";
  public static final String SIGNING_KEY_KEYSTORE                      = "pathToSigningKey";
  public static final String SIGNING_KEY_KEYSTORE_PASSWORD             = "signingKeyPwd";
  public static final String SIGNING_KEY_SK1_PASSWORD                  = "sk1Pwd";
  public static final String SIGNING_KEY_SK1_SUFFIX                    = SystemConstants.SIGNING_KEY_SK1_SUFFIX;
  public static final String SIGNING_KEY_SK2_PASSWORD                  = "sk2Pwd";
  public static final String SIGNING_KEY_SK2_SUFFIX                    = SystemConstants.SIGNING_KEY_SK2_SUFFIX;
  public static final String SOCKET_TIMEOUT_INITIAL                    = "socketTimeoutInitial";
  public static final String SOCKET_TIMEOUT_MAX                        = "socketTimeoutMax";
  public static final String SSL_CIPHER_DEFAULT                        = "default";
  public static final String SSL_CONFIG                                = "SSLConfig";
  public static final String SSL_KEY_SUFFIX                            = "_SSL";
  public static final String SSL_TIMEOUT_INITIAL                       = "sslTimeoutInitial";
  public static final String SSL_TIMEOUT_MAX                           = "sslTimeoutMax";
  public static final String SUBMISSION_TIMEOUT                        = "submissionTimeout";
  public static final String THRESHOLD                                 = "threshold";
  public static final String TIME_STRING_SEPARATOR                     = ":";
  public static final String UPLOAD_DIR                                = "uploadDirectory";

  /**
   * Checks whether the system should be performing live checking of ballot reductions
   * 
   * @return boolean true if live checking should be performed, false if not
   */
  public abstract boolean doLivePODCheck();

  /**
   * Gets a JSONArray of base encrypted Candidate IDs. These are the IDs that are encrypted with a fixed randomness and are used as
   * the input to the ballot generation. These are required for live checking the ballot reduction.
   * 
   * @return JSONArray containing the base encrypted Candidate IDs
   */
  public abstract JSONArray getBaseEncryptedIDs();

  /**
   * @return The buffer bound.
   */
  public abstract int getBufferBound();

  /**
   * @return The district configuration.
   */
  public abstract JSONObject getDistrictConf();

  /**
   * Gets the instance of ECUtils used throughout the MBB. This saves having to construct multiple instances across the other
   * classes
   * 
   * @return ECUtils instance
   */
  public abstract ECUtils getECUtils();

  /**
   * Gets the election public key - this is important for performing live ballot reduction
   * 
   * @return ECPoint containing the Election Public Key
   */
  public abstract ECPoint getElectionPublicKey();

  /**
   * Gets the JSON object for the requested host
   * 
   * @param id
   *          PeerID of the required host
   * @return JSONObject containing host address and port
   */
  public abstract JSONObject getHostDetails(String id);

  /**
   * Gets an int property value
   * 
   * @param key
   *          name of property to get
   * @return int value of the requested property or 0 if it does not exist or is not an integer
   */
  public abstract int getInt(String key);

  /**
   * @return The message digest.
   * @throws NoSuchAlgorithmException
   */
  public abstract MessageDigest getMessageDigest() throws NoSuchAlgorithmException;

  /**
   * @return The map of peers.
   */
  public abstract HashMap<String, JSONObject> getPeers();

  /**
   * Gets an individual RaceDef from the preloaded RaceDefs that matches the string id.
   * 
   * @param id
   *          String of race ID - case sensitive
   * @return RaceDef matching id or null if not found
   */
  public abstract RaceDef getRaceDef(String id);

  /**
   * Gets all RaceDefs that have been loaded
   * 
   * @return RaceDefs containing all loaded race defintions
   */
  public abstract RaceDefs getRaceDefs();

  /**
   * Gets a list of the loaded RaceIds
   * 
   * @return List of strings containing loaded race ids from RaceDefs
   */
  public abstract List<String> getRaceIds();

  /**
   * Gets the integer array of race sizes<br>
   * 
   * 
   * @return integer array of races sizes
   */
  public abstract int[] getRaceSizes();

  /**
   * Gets the initial wait time for a general socket error
   * 
   * @return milliseconds to wait before retrying
   */
  public abstract int getSocketFailTimeoutInitial();

  /**
   * Get the maximum amount of time to wait after successive socket failures
   * 
   * @return milliseconds to wait before retrying
   */
  public abstract int getSocketFailTimeoutMax();

  /**
   * Gets a string property value
   * 
   * @param key
   *          name of property to get
   * @return String value of the property or empty string if property does not exist or is not a string
   */
  public abstract String getString(String key);

  /**
   * Gets the submission timeout - period to wait before return error to client
   * 
   * @return seconds to wait before timing out
   */
  public abstract int getSubmissionTimeout();

  /**
   * @return The threshold.
   */
  public abstract int getThreshold();

  /**
   * Gets requested time property extracts the requested TimeUnit from it
   * 
   * @param key
   *          name of property to get from config
   * @param unit
   *          TimeUnit (Hours or Minutes) to return
   * @return int representing the relevant time unit or -1 if invalid TimeUnit or key is requested
   */
  public abstract int getTime(String key, TimeUnit unit);

}