/*******************************************************************************
 * Copyright (c) 2013 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.config;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.conf.RaceDef;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileChangeListener;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileSystemWatcher;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileWatcherException;

/**
 * Provides access to the JSON Configuration for this peer.
 * 
 * A number of values are pre-loaded and cached to save having to read them every time.
 * 
 * @author Chris Culnane
 * 
 */
public class AutoReloadWBBConfig implements FileChangeListener, WBBConfig {

  public static final AutoReloadWBBConfig _instance                                 = new AutoReloadWBBConfig();
  /**
   * Logger
   */
  private static final Logger   logger                                    = LoggerFactory.getLogger(AutoReloadWBBConfig.class);
  /**
   * Gets the singleton instance of the WBBConfig
   * 
   * @return WBBConfig instance
   */
  public static final AutoReloadWBBConfig getInstance() {
    return _instance;
  }

  /**
   * JSONArray of base encrypted candidate IDs - these are the candidate IDs encrypted with a fixed randomness and used during
   * ballot generation
   */
  private volatile JSONArray                   baseEncryptedIDs;

  /**
   * The maximum size allowed for the read Buffer, this is effectively the maximum amount of data that can be sent in a message
   */
  private volatile int                         bufferBound;

  /**
   * Underlying JSON object that contains the configuration file
   */
  private volatile JSONObject                  config;

  /**
   * File that points to config file JSON
   */
  private File                                 configFile;

  /**
   * String path to config file
   */
  private String                               configFilePath;
  /**
   * JSONObject with system wide district configuration, which specifies the number of candidates in each district and race
   */
  private volatile JSONObject                  districtConf;

  /**
   * File that points to the district config file
   */
  private File                                 districtConfFile;

  /**
   * Instance of ECUtils to be used throughout - saves having to construct multiple instances
   */
  private volatile ECUtils                     ecUtils                  = new ECUtils();

  /**
   * ECPoint containing the election public key
   */
  private volatile ECPoint                     electionPublicKey;

  /**
   * FileSystemWatcher to monitor and reload the config file when it changes
   */
  private FileSystemWatcher                    fsw;

  /**
   * boolean that determines whether to check ballot reductions in real time
   */
  private volatile boolean                     liveCheckPOD             = false;

  /**
   * Stores a JSON object for each Peer listed in the config file. The JSON object provides access to the address and port of the
   * peer. It is indexed by the Peer ID
   */
  private volatile HashMap<String, JSONObject> peers                    = new HashMap<String, JSONObject>();

  /**
   * RaceDefs contains the definitions and validation for each race
   */
  private volatile RaceDefs                    raceDefs                 = new RaceDefs();

  /**
   * integer array to pre-cache the sizes of the races, used during Ballot Reduction
   */
  private volatile int[]                       raceSizes                = null;

  /**
   * Initial timeout for a generic socket failure. This is not an an uncommon occurrence, for example, during startup peers will be
   * unable to connect to each other until they are all running. This time represents the initial wait period following an error.
   * Like the SSL Wait time it will grow logarithmically up to the max time.
   */
  private volatile int                         socketFailTimeoutInitial = 2000;
  /**
   * Max delay the socket will wait before trying to reconnect. This should not be too large, since it is not uncommon for socket
   * errors to occur. This delay will determine the maximum amount of time that will need to be waited until the connection is tried
   * again.
   */
  private volatile int                         socketFailTimeoutMax     = 2000;

  /**
   * Represents the period within which a message should have reached consensus across the peers and a response sent. If it expires
   * a failure message will be returned to the client.
   */
  private volatile int                         submissionTimeout        = 30;

  /**
   * int of the threshold of valid responses required - must be strictly more than 2/3 of the number of peers
   */
  private volatile int                         threshold;

  /**
   * Empty constructor used during single instance initialisation
   */
  private AutoReloadWBBConfig() {

  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#doLivePODCheck()
   */
  @Override
  public boolean doLivePODCheck() {
    return this.liveCheckPOD;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.tvs.cs.utils.fswatcher.FileChangeListener#fileChanged(java.io.File)
   */
  @Override
  public synchronized void fileChanged(File filePath) {
    try {

      logger.info("File change event has occurred:{}", filePath.getCanonicalPath());
      if (filePath.getCanonicalFile().equals(this.configFile.getCanonicalFile())) {
        logger.info("Reloading WBB config");
        this.loadConfig(this.configFilePath);
      }
      else if (filePath.getCanonicalFile().equals(this.districtConfFile.getCanonicalFile())) {
        try {
          logger.info("Reloading district config");
          this.districtConf = IOUtils.readJSONObjectFromFile(this.districtConfFile.getAbsolutePath());
        }
        catch (JSONIOException e) {
          logger.warn("Exception whilst reloading DistrictConf - could be a partial write");
        }
      }
      else {
        logger.warn("Unknown file change event:{}", filePath);
      }

    }
    catch (JSONSchemaValidationException e) {
      logger.warn("Exception whilst validating Config file - could be a partial write");
    }
    catch (IOException e) {
      logger.warn("IOException in fileChanged method", e);
    }

  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getBaseEncryptedIDs()
   */
  @Override
  public JSONArray getBaseEncryptedIDs() {
    return this.baseEncryptedIDs;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getBufferBound()
   */
  @Override
  public int getBufferBound() {
    return this.bufferBound;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getDistrictConf()
   */
  @Override
  public JSONObject getDistrictConf() {
    return this.districtConf;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getECUtils()
   */
  @Override
  public ECUtils getECUtils() {
    return this.ecUtils;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getElectionPublicKey()
   */
  @Override
  public ECPoint getElectionPublicKey() {
    return this.electionPublicKey;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getHostDetails(java.lang.String)
   */
  @Override
  public JSONObject getHostDetails(String id) {
    return this.peers.get(id);
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getInt(java.lang.String)
   */
  @Override
  public int getInt(String key) {
    try {
      return this.config.getInt(key);
    }
    catch (JSONException e) {
      logger.error("WBBConfig JSON Exception when trying to get {} from WBBConfig", key, e);
    }
    return 0;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getMessageDigest()
   */
  @Override
  public MessageDigest getMessageDigest() throws NoSuchAlgorithmException {
    if (this.config.has(WBBConfig.MESSAGE_DIGEST)) {
      try {
        return MessageDigest.getInstance(this.config.getString(WBBConfig.MESSAGE_DIGEST));
      }
      catch (JSONException e) {
        logger.error("Exception whilst getting message digest from WBB Config. Using {} instead", WBBConfig.DEFAULT_MESSAGE_DIGEST,
            e);
        return MessageDigest.getInstance(WBBConfig.DEFAULT_MESSAGE_DIGEST);
      }
    }
    else {
      return MessageDigest.getInstance(WBBConfig.DEFAULT_MESSAGE_DIGEST);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getPeers()
   */
  @Override
  public HashMap<String, JSONObject> getPeers() {
    return this.peers;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getRaceDef(java.lang.String)
   */
  @Override
  public RaceDef getRaceDef(String id) {
    return this.raceDefs.getRaceDef(id);
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getRaceDefs()
   */
  @Override
  public RaceDefs getRaceDefs() {
    return this.raceDefs;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getRaceIds()
   */
  @Override
  public List<String> getRaceIds() {
    return this.raceDefs.getRaceIds();
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getRaceSizes()
   */
  @Override
  public int[] getRaceSizes() {

    return this.raceSizes;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getSocketFailTimeoutInitial()
   */
  @Override
  public int getSocketFailTimeoutInitial() {
    return this.socketFailTimeoutInitial;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getSocketFailTimeoutMax()
   */
  @Override
  public int getSocketFailTimeoutMax() {
    return this.socketFailTimeoutMax;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getString(java.lang.String)
   */
  @Override
  public String getString(String key) {
    try {
      return this.config.getString(key);
    }
    catch (JSONException e) {
      logger.error("WBBConfig JSON Exception when trying to get {} from WBBConfig", key, e);
    }
    return null;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getSubmissionTimeout()
   */
  @Override
  public int getSubmissionTimeout() {
    return this.submissionTimeout;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getThreshold()
   */
  @Override
  public int getThreshold() {
    return this.threshold;
  }

  /* (non-Javadoc)
   * @see uk.ac.surrey.cs.tvs.wbb.config.WBBConfigInterface#getTime(java.lang.String, java.util.concurrent.TimeUnit)
   */
  @Override
  public int getTime(String key, TimeUnit unit) {
    if (this.config.has(key)) {
      try {
        if (unit == TimeUnit.HOURS) {
          String value = this.config.getString(key);
          return Integer.parseInt(value.substring(0, value.indexOf(WBBConfig.TIME_STRING_SEPARATOR)));
        }
        else if (unit == TimeUnit.MINUTES) {
          String value = this.config.getString(key);
          return Integer.parseInt(value.substring(value.indexOf(WBBConfig.TIME_STRING_SEPARATOR) + 1));
        }
        else {
          return -1;
        }
      }
      catch (JSONException e) {
        logger.error("Exception whilst getting time from WBB Config", e);
      }
    }

    return -1;
  }

  /**
   * Initialises the WBBConfig with file specified
   * 
   * @param file
   *          String containing path to config file JSON
   * @throws JSONSchemaValidationException
   */
  public synchronized void initConfig(String file) throws JSONSchemaValidationException {
    try {
      logger.info("Starting load of configuration file: {}", file);
      this.configFilePath = file;

      this.configFile = new File(this.configFilePath);
      if (!this.configFile.exists()) {
        logger.error("Failed to find WBBConfig");
      }
      this.fsw = new FileSystemWatcher(this.configFile.getCanonicalFile().getParentFile());
      this.fsw.addFileListener(this.configFile.getCanonicalFile(), this);

      this.loadConfig(this.configFilePath);

    }
    catch (IOException e) {
      logger.error("Failed to load WBBConfig", e);
    }

    catch (FileWatcherException e) {
      logger.error("Exception during setting up FileSystemWatcher on config file");
    }
  }

  /**
   * Loads the actual config file, this is separate to the initConfig so that it can be called during file updates as well as during
   * initialisation.
   * 
   * @param file
   *          String containing path to config file
   * @throws JSONSchemaValidationException
   */
  private synchronized void loadConfig(String file) throws JSONSchemaValidationException {
    try {
      logger.info("Starting to load config file:", file);
      // Loads the configuration file - this is a trusted file so we don't use a bounded reader object
      String configString = IOUtils.readStringFromFile(file);

      // Check the config file validates
      if (!JSONUtils.validateSchema(JSONUtils.loadSchema(WBBConfig.SCHEMA_LOCATION), configString)) {
        throw new JSONSchemaValidationException("Config file failed schema validation");
      }
      logger.info("Config file is valid");
      // Load the string into a JSON object
      this.config = new JSONObject(configString);

      this.bufferBound = this.config.getInt(WBBConfig.BUFFER_BOUND);
      this.threshold = this.config.getInt(WBBConfig.THRESHOLD);
      // Load the district configuration

      if (this.districtConfFile != null) {
        logger.info("Removing old district config listener");
        this.fsw.removeFileListener(this.districtConfFile, this);
      }
      this.districtConfFile = new File(this.config.getString(WBBConfig.DISTRICT_CONFIG));
      logger.info("Adding district config listener");
      this.fsw.addFileListener(this.districtConfFile, this);
      logger.info("About to load district config file");
      this.districtConf = IOUtils.readJSONObjectFromFile(this.districtConfFile.getAbsolutePath());
      // Extract the peers from the configuration file
      JSONArray hostArray = this.config.getJSONArray(WBBConfig.PEERS);
      for (int i = 0; i < hostArray.length(); i++) {
        JSONObject host = hostArray.getJSONObject(i);
        String hostIndex = host.getString(WBBConfig.PEER_ID);
        this.peers.put(hostIndex, host);
      }
      this.electionPublicKey = this.ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(this.config
          .getString(WBBConfig.ELECTION_PUBLIC_KEY)));
      this.baseEncryptedIDs = IOUtils.readJSONArrayFromFile(this.config.getString(WBBConfig.BASE_ENCRYPTED_IDS_FILE));

      // Set hard-coded properties from the config file
      this.socketFailTimeoutInitial = this.config.getInt(WBBConfig.SOCKET_TIMEOUT_INITIAL);
      this.socketFailTimeoutMax = this.config.getInt(WBBConfig.SOCKET_TIMEOUT_MAX);
      this.submissionTimeout = this.config.getInt(WBBConfig.SUBMISSION_TIMEOUT);
      this.liveCheckPOD = this.config.getBoolean(WBBConfig.LIVE_CHECK_POD);
      JSONArray raceArr = this.config.getJSONArray(WBBConfig.RACES);
      this.raceSizes = new int[raceArr.length()];

      RaceDefs raceDefs = new RaceDefs();
      for (int i = 0; i < raceArr.length(); i++) {
        RaceDef raceDef = new RaceDef(raceArr.getJSONObject(i));
        raceDefs.addRaceDef(raceDef);
        JSONObject raceObj = raceArr.getJSONObject(i);
        this.raceSizes[i] = raceObj.getInt(WBBConfig.CANDIDATE_COUNT);
      }
      this.raceDefs = raceDefs;
      logger.info("Finished loading Peer config: {}", file);
    }
    catch (IOException e) {
      logger.error("Failed to load WBBConfig", e);
    }
    catch (JSONException e) {
      logger.error("WBBConfig is malformed", e);
    }
    catch (JSONIOException e) {
      logger.error("JSON Config file is malformed", e);
    }

  }

  /**
   * Starts the monitoring of the underlying config file for changes. If called stopMonitoring should be called to shutdown the
   * monitor and destroy the thread during shutdown.
   */
  public synchronized void startMonitoring() {
    if (this.fsw != null) {
      try {
        this.fsw.startWatching();
      }
      catch (FileWatcherException e) {
        logger.error("Exception whilst starting file system watcher", e);
      }
    }
  }

  /**
   * Stops the monitoring of underlying config file
   */
  public synchronized void stopMonitoring() {
    if (this.fsw != null) {
      try {
        this.fsw.stopWatching();
      }
      catch (FileWatcherException e) {
        logger.error("Exception whilst stopping file system watcher", e);
      }
    }
  }
}
