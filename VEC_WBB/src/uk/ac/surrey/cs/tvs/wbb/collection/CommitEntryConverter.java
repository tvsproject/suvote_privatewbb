/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb.collection;

import java.util.Arrays;
import java.util.Comparator;

import uk.ac.surrey.cs.tvs.collections.MappedFileArrayEntryConverter;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;

/**
 * Converter to encode and decode Commit messages (JSONWBBMessages) to and from byte arrays for use with the MappedFileArray.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitEntryConverter extends MappedFileArrayEntryConverter<JSONWBBMessage> {

  private static final char SERIAL_NO_SEPARATOR = ':';

  /**
   * Basic length is zero
   */
  private int               initialLineLength   = 0;

  /**
   * Empty constructor
   */
  public CommitEntryConverter() {

  }

  /**
   * Constructor that creates a CommitEntryConverter with the specified initial line length
   * 
   * @param initialLineLength
   *          int length of entries
   */
  public CommitEntryConverter(int initialLineLength) {
    this.initialLineLength = initialLineLength;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(byte[] o1, byte[] o2) {
    return this.doCompare(o1, o2);
  }

  private int compareByteArrays(byte[] o1, byte[] o2) {
    int lenDif = o1.length - o2.length;

    if (lenDif != 0) {
      return lenDif;
    }
    else {
      // Step through the arrays checking each value, if any aren't equal which ever is larger is bigger
      for (int i = 0; i < o1.length; i++) {
        int dif = o1[i] - o2[i];

        if (dif != 0) {
          return dif;
        }
      }
    }

    // If we get here it means they are equal
    return 0;
  }

  private int compareStringByteArrays(byte[] o1, byte[] o2, int starto1, int starto2, int lengtho1, int lengtho2) {

    // Step through the arrays checking each value, if any aren't equal which ever is larger is bigger
    for (int i = 0; i < Math.max(lengtho1, lengtho2); i++) {
      if (i > lengtho1) {
        return -1;
      }
      else if (i > lengtho2) {
        return 1;
      }

      int dif = o1[starto1 + i] - o2[starto2 + i];

      if (dif != 0) {
        return dif;
      }

    }

    // If we get here it means they are equal
    return 0;
  }

  private byte[] convertToByteArray(JSONWBBMessage entry) {
    String serial = entry.getID();
    byte[] serialRaceID;
    int serialBallotNum = 0;
    boolean isSerialSplit;
    byte[] message = entry.toString().getBytes();
    if (serial.indexOf(SERIAL_NO_SEPARATOR) >= 0) {
      serialRaceID = serial.substring(0, serial.indexOf(SERIAL_NO_SEPARATOR)).getBytes();
      serialBallotNum = Integer.parseInt(serial.substring(serial.indexOf(SERIAL_NO_SEPARATOR) + 1));
      isSerialSplit = true;
    }
    else {
      isSerialSplit = false;
      serialRaceID = serial.getBytes();
    }
    byte[] output = new byte[11 + serialRaceID.length + message.length];
    output[0] = (byte) entry.getType().ordinal();
    output[1] = (byte) (isSerialSplit ? 1 : 0);
    output[2] = (byte) serialRaceID.length;
    int idx = 3;
    for (int i = 0; idx < 3 + serialRaceID.length; idx++, i++) {
      output[idx] = serialRaceID[i];
    }
    if (isSerialSplit) {
      output[idx] = (byte) (serialBallotNum >> 24);
      output[idx + 1] = (byte) (serialBallotNum >> 16);
      output[idx + 2] = (byte) (serialBallotNum >> 8);
      output[idx + 3] = (byte) (serialBallotNum);
      idx = idx + 4;
    }
    int messagelength = message.length;
    output[idx] = (byte) (messagelength >> 24);
    output[idx + 1] = (byte) (messagelength >> 16);
    output[idx + 2] = (byte) (messagelength >> 8);
    output[idx + 3] = (byte) (messagelength);
    idx = idx + 4;
    for (int i = 0; i < messagelength; idx++, i++) {
      output[idx] = message[i];
    }
    return output;

  }

  private JSONWBBMessage convertToJSONWBBMessage(byte[] entry) {
    int idx = 3 + entry[2] + 4;
    int messagelength = ((entry[idx] & 0xFF) << 24) | ((entry[idx + 1] & 0xFF) << 16) | ((entry[idx + 2] & 0xFF) << 8)
        | (entry[idx + 3] & 0xFF);
    idx = idx + 4;
    byte[] message = Arrays.copyOfRange(entry, idx, idx + messagelength);

    try {
      return JSONWBBMessage.parseMessage(new String(message));
    }
    catch (MessageJSONException e) {
      throw new RuntimeException("Cannot decode", e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.collection.MappedFileArrayEntryConverter#decode(byte[])
   */
  @Override
  public JSONWBBMessage decode(byte[] entry) {
    return this.convertToJSONWBBMessage(entry);
  }

  /**
   * Compares to byte arrays that contain CommitEntries that have been encoded as byte arrays
   * @param o1 byte array of CommitEntry A
   * @param o2 byte array of CommitEntry B
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
   */
  public int doCompare(byte[] o1, byte[] o2) {

    // Messages are ordered initially by type and then by ID
    if (o1[0] == o2[0]) {// compare type
      if ((o1[1] != 0) && (o2[1] != 0)) {// is serial number split

        int comp = this.compareStringByteArrays(o1, o2, 3, 3, o1[2], o2[2]);// Compare raceID
        if (comp == 0) {
          return getIntFromBytes(o1, 3 + o1[2]) - getIntFromBytes(o2, 3 + o2[2]);
        }
        else {
          return comp;
        }
      }
      else {
        return this.compareStringByteArrays(o1, o2, 3, 3, o1[2], o2[2]);
      }
    }
    else {
      return o1[0] - o2[0];
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.collection.MappedFileArrayEntryConverter#encode(java.lang.Object)
   */
  @Override
  public byte[] encode(JSONWBBMessage entry) {
    return this.convertToByteArray(entry);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.collection.MappedFileArrayEntryConverter#getComparator()
   */
  @Override
  public Comparator<byte[]> getComparator() {
    return this;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.collection.MappedFileArrayEntryConverter#getDefaultLineLength()
   */
  @Override
  public int getInitialLineLength() {
    return this.initialLineLength;
  }

  /**
   * Converts a byte array to an integer
   * 
   * @param b
   *          byte array containing an integer
   * @param start
   *          int index of start point in array
   * @return int decoded from byte array
   */
  private int getIntFromBytes(byte[] b, int start) {
    return ((b[start] & 0xFF) << 24) | ((b[start + 1] & 0xFF) << 16) | ((b[start + 2] & 0xFF) << 8) | (b[start + 3] & 0xFF);
  }

}
