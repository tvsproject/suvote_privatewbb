/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.wbb;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.conf.RaceDef;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.wbb.config.AutoReloadWBBConfig;
import uk.ac.surrey.cs.tvs.wbb.config.WBBConfig;
import uk.ac.surrey.cs.tvs.wbb.exceptions.JSONSchemaValidationException;

/**
 * This class defines a dummy WBBConfig.
 */
public class DummyWBBConfig implements WBBConfig {

  /** Map holding the dummy configuration values. */
  Map<String, Object> map = new HashMap<String, Object>();

  
  /**
   * Default constructor which initialises the configuration without a config file.
   * 
   * @throws JSONSchemaValidationException
   */
  public DummyWBBConfig() {
    
  }

  /**
   * Gets an int property value
   * 
   * @param key
   *          name of property to get
   * @return int value of the requested property or 0 if it does not exist or is not an integer
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.config.AutoReloadWBBConfig#getInt(java.lang.String)
   */
  public int getInt(String key) {
    // Only return what has been put into the map.
    return (int) this.map.get(key);
  }

  /**
   * Gets a string property value
   * 
   * @param key
   *          name of property to get
   * @return String value of the property or empty string if property does not exist or is not a string
   * 
   * @see uk.ac.surrey.cs.tvs.wbb.config.AutoReloadWBBConfig#getString(java.lang.String)
   */
  public String getString(String key) {
    // Only return what has been put into the map.
    return (String) this.map.get(key);
  }

  /**
   * Puts configuration information into the object.
   * 
   * @param key
   *          The configuration key.
   * @param value
   *          The value.
   */
  public void put(String key, Object value) {
    this.map.put(key, value);
  }

  @Override
  public boolean doLivePODCheck() {
    return AutoReloadWBBConfig.getInstance().doLivePODCheck();
  }

  @Override
  public JSONArray getBaseEncryptedIDs() {
    return AutoReloadWBBConfig.getInstance().getBaseEncryptedIDs();
  }

  @Override
  public int getBufferBound() {
    return AutoReloadWBBConfig.getInstance().getBufferBound();
  }

  @Override
  public JSONObject getDistrictConf() {
    return AutoReloadWBBConfig.getInstance().getDistrictConf();
  }

  @Override
  public ECUtils getECUtils() {
    return AutoReloadWBBConfig.getInstance().getECUtils();
  }

  @Override
  public ECPoint getElectionPublicKey() {
    return AutoReloadWBBConfig.getInstance().getElectionPublicKey();
  }

  @Override
  public JSONObject getHostDetails(String id) {
    return AutoReloadWBBConfig.getInstance().getHostDetails(id);
  }

  @Override
  public MessageDigest getMessageDigest() throws NoSuchAlgorithmException {
    return AutoReloadWBBConfig.getInstance().getMessageDigest();
  }

  @Override
  public HashMap<String, JSONObject> getPeers() {
    return AutoReloadWBBConfig.getInstance().getPeers();
  }

  @Override
  public RaceDef getRaceDef(String id) {
    return AutoReloadWBBConfig.getInstance().getRaceDef(id);
  }

  @Override
  public RaceDefs getRaceDefs() {
    return AutoReloadWBBConfig.getInstance().getRaceDefs();
  }

  @Override
  public List<String> getRaceIds() {
    return AutoReloadWBBConfig.getInstance().getRaceIds();
  }

  @Override
  public int[] getRaceSizes() {
    return AutoReloadWBBConfig.getInstance().getRaceSizes();
  }

  @Override
  public int getSocketFailTimeoutInitial() {
    return AutoReloadWBBConfig.getInstance().getSocketFailTimeoutInitial();
  }

  @Override
  public int getSocketFailTimeoutMax() {
    return AutoReloadWBBConfig.getInstance().getSocketFailTimeoutMax();
  }

  @Override
  public int getSubmissionTimeout() {
    return AutoReloadWBBConfig.getInstance().getSubmissionTimeout();
  }

  @Override
  public int getThreshold() {
    return AutoReloadWBBConfig.getInstance().getThreshold();
  }

  @Override
  public int getTime(String key, TimeUnit unit) {
    return AutoReloadWBBConfig.getInstance().getTime(key,unit);
  }

}
